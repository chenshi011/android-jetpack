# Android Jetpack

#### 项目介绍
Android Jetpack 支持库测试


#### 使用说明
- com.mazaiting.jetpack
	- architecture: 架构包  
		- databinding: 数据绑定框架 -- [简书地址](https://www.jianshu.com/p/adcc92f26b69)
			- bean: 实体包
			- expression: 表达式包
			- observable: 观察者包
		- lefecycle: 声明周期框架，解决Activity和Fragment MVP架构内存泄漏 -- [简书地址](https://www.jianshu.com/p/9d500a3fb2d7)
		- live: LiveData框架，解决内存泄漏等问题 -- [简书地址](https://www.jianshu.com/p/4f7b5f47c7fa)
		- model: ViewModel框架，解决Activity横竖屏切换数据丢失问题，Fragment之间数据传递更优化 -- [简书地址](https://www.jianshu.com/p/366c29eee33a)
		- paging: 分页加载框架 -- [简书地址](https://www.jianshu.com/p/426de12e2fe8)
		- room: 数据库操作框架  -- [简书地址](https://www.jianshu.com/p/eb2377bfced2)
		- work: 任务管理框架 -- [简书地址](https://www.jianshu.com/p/849424883aee)
		- ArchitectureActivity.kt 架构主界面
	- behavior: 行为
	- foundation: 基础
	- ui: 界面
	- MainActivity.kt: 应用主入口