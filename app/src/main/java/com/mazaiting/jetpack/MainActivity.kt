package com.mazaiting.jetpack

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.mazaiting.jetpack.architecture.ArchitectureActivity
import com.mazaiting.jetpack.behavior.BehaviorActivity
import com.mazaiting.jetpack.foundation.FoundationActivity
import com.mazaiting.jetpack.ui.UIActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
  
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
    
    btn_foundation.setOnClickListener { startActivity(Intent(this, FoundationActivity::class.java)) }
    btn_architecture.setOnClickListener { startActivity(Intent(this, ArchitectureActivity::class.java)) }
    btn_behavior.setOnClickListener { startActivity(Intent(this, BehaviorActivity::class.java)) }
    btn_ui.setOnClickListener { startActivity(Intent(this, UIActivity::class.java)) }
  }
}
