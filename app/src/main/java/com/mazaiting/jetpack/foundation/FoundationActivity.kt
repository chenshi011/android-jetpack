package com.mazaiting.jetpack.foundation

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.mazaiting.jetpack.R

class FoundationActivity : AppCompatActivity() {
  
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_foundation)
  }
}
