package com.mazaiting.jetpack.architecture.paging

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.mazaiting.jetpack.R
import com.mazaiting.jetpack.architecture.paging.custom.PagingCustomActivity
import com.mazaiting.jetpack.architecture.paging.room.PagingRoomActivity
import kotlinx.android.synthetic.main.activity_paging.*

class PagingActivity : AppCompatActivity() {
  /**
   * 懒加载ViewModel
   */
  private val viewModel by lazy {
    ViewModelProviders.of(this).get(StudentViewModel::class.java)
  }
  
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_paging)
    // 向ROOM中添加数据
    btn_paging_add.setOnClickListener { viewModel.save(this.application) }
    // 读取ROOM中的数据并显示
    btn_paging_room.setOnClickListener { startActivity(Intent(this, PagingRoomActivity::class.java)) }
    // 读取网络数据并显示
    btn_paging_net.setOnClickListener { startActivity(Intent(this, PagingCustomActivity::class.java)) }
  }
}
