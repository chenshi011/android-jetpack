package com.mazaiting.jetpack.architecture.lifecycle.custom

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.mazaiting.jetpack.R

class CustomLifeCycleActivity : AppCompatActivity() {
  private lateinit var presenter: IPresenter
  
  companion object {
    private val TAG = CustomLifeCycleActivity::class.java.simpleName
  }
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_custom_life_cyclle)
    presenter = CustomPresenter()
    Log.e(TAG, "-- onCreate")
    presenter.onCreate()
  }
  
  override fun onStart() {
    super.onStart()
    Log.e(TAG, "-- onStart")
    presenter.onStart()
  }
  
  override fun onResume() {
    super.onResume()
    Log.e(TAG, "-- onResume")
    presenter.onResume()
  }
  
  override fun onPause() {
    super.onPause()
    Log.e(TAG, "-- onPause")
    presenter.onPause()
  }
  
  override fun onStop() {
    super.onStop()
    Log.e(TAG, "-- onStop")
    presenter.onStop()
  }
  
  override fun onDestroy() {
    super.onDestroy()
    Log.e(TAG, "-- onDestroy")
    presenter.onDestroy()
  }
}
