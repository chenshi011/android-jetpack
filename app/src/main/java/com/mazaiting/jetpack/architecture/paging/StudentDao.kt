package com.mazaiting.jetpack.architecture.paging

import android.arch.paging.DataSource
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

/**
 * 数据库操作对象
 * Created by mazaiting on 2018/7/26.
 */
@Dao
interface StudentDao {
  /**
   * 插入数据
   */
  @Insert
  fun insert(student: List<StudentRoom>)
  
  /**
   * 查询所有学生
   */
  @Query("SELECT * FROM student ORDER BY id DESC")
  fun findAllStudents() : DataSource.Factory<Int, StudentRoom>
}