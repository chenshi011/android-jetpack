package com.mazaiting.jetpack.architecture.lifecycle.life

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.OnLifecycleEvent
import org.jetbrains.annotations.NotNull

/**
 * Presenter接口
 * Created by mazaiting on 2018/7/27.
 */
interface IPresenter : LifecycleObserver {
  @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
  fun onCreate(@NotNull owner: LifecycleOwner)
  
  @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
  fun onDestroy(@NotNull owner: LifecycleOwner)
  
  @OnLifecycleEvent(Lifecycle.Event.ON_ANY)
  fun onLifecycleChanged(@NotNull owner: LifecycleOwner)
}