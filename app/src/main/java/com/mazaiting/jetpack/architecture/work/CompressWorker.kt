package com.mazaiting.jetpack.architecture.work

import androidx.work.Worker
import com.mazaiting.log.L


/**
 * Created by mazaiting on 2018/7/25.
 */
class CompressWorker : Worker() {
  override fun doWork(): WorkerResult {
    L.e("mazaiting")
    return WorkerResult.SUCCESS
  }
  
}