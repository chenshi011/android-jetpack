package com.mazaiting.jetpack.architecture.databinding.expression

import android.view.View
import com.mazaiting.jetpack.architecture.databinding.bean.User
import com.mazaiting.log.L

/**
 * 主持人
 * Created by mazaiting on 2018/7/24.
 */
class ExpressionPresenter {
  /**
   * 方法监听
   */
  fun show(view: View, user: User) {
    L.e(user.firstName + user.lastName)
  }
  
  /**
   * 方法引用
   */
  fun onClickShow(view: View) {
    L.e("方法引用")
  }
}