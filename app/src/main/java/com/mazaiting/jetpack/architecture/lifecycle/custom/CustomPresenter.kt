package com.mazaiting.jetpack.architecture.lifecycle.custom

import android.util.Log

/**
 * Created by mazaiting on 2018/7/27.
 */
class CustomPresenter : IPresenter {
  companion object {
    private val TAG = CustomPresenter::class.java.simpleName
  }
  override fun onCreate() {
    Log.e(TAG, "-- onCreate")
  }
  override fun onStart() {
    Log.e(TAG, "-- onStart")
  }
  override fun onResume() {
    Log.e(TAG, "-- onResume")
  }
  override fun onPause() {
    Log.e(TAG,"-- onPause")
  }
  override fun onStop() {
    Log.e(TAG, "-- onStop")
  }
  override fun onDestroy() {
    Log.e(TAG, "-- onDestroy")
  }
}