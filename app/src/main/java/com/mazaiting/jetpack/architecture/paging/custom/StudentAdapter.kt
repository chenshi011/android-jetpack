package com.mazaiting.jetpack.architecture.paging.custom

import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.mazaiting.jetpack.R
import com.mazaiting.jetpack.architecture.paging.StudentRoom

/**
 * 适配器
 * Created by mazaiting on 2018/7/26.
 */
class StudentAdapter : PagedListAdapter<Student, StudentAdapter.StudentViewHolder>(DIFF_CALLBACK) {
  override fun onBindViewHolder(holder: StudentViewHolder, position: Int) =
          holder.bindTo(getItem(position))
  
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentViewHolder =
          StudentViewHolder(parent)
  
  companion object {
    /**
     * 条目不同回调
     */
    private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Student>() {
      override fun areItemsTheSame(oldItem: Student, newItem: Student): Boolean = oldItem.id == newItem.id
      
      override fun areContentsTheSame(oldItem: Student, newItem: Student): Boolean = oldItem == newItem
    }
  }
  
  class StudentViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
          // 加载布局
          LayoutInflater.from(parent.context).inflate(R.layout.item_student, parent, false)
  ) {
    // 获取view
    private val nameView = itemView.findViewById<TextView>(R.id.tv_name)
    private var student: Student? = null
  
    /**
     * 绑定数据
     */
    fun bindTo(student: Student?) {
      this.student = student
      nameView.text = student?.name
    }
  }
}