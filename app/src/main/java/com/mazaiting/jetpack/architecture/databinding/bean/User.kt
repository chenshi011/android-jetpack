package com.mazaiting.jetpack.architecture.databinding.bean

/**
 * 用户实体类
 * Created by mazaiting on 2018/7/24.
 */
data class User (
        var firstName: String,
        val lastName: String,
        val index: Int,
        val display: String?,
        val iconName: String = "launcher"
)