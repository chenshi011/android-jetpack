package com.mazaiting.jetpack.architecture.paging

import android.app.Application
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.DataSource
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import android.arch.persistence.room.Room
import com.mazaiting.jetpack.architecture.paging.custom.Student
import com.mazaiting.jetpack.architecture.paging.custom.StudentDataSource

private val CHEESE_DATA_STRING = arrayListOf(
        "Abbaye de Belloc", "Abbaye du Mont des Cats", "Abertam", "Abondance", "Ackawi",
        "Acorn", "Adelost", "Affidelice au Chablis", "Afuega'l Pitu", "Airag", "Airedale",
        "Aisy Cendre", "Allgauer Emmentaler", "Alverca", "Ambert", "American Cheese",
        "Ami du Chambertin", "Anejo Enchilado", "Anneau du Vic-Bilh", "Anthoriro", "Appenzell",
        "Aragon", "Ardi Gasna", "Ardrahan", "Armenian String", "Aromes au Gene de Marc",
        "Asadero", "Asiago", "Aubisque Pyrenees", "Autun", "Avaxtskyr", "Baby Swiss",
        "Babybel", "Baguette Laonnaise", "Bakers", "Baladi", "Balaton", "Bandal", "Banon",
        "Barry's Bay Cheddar", "Basing", "Basket Cheese", "Bath Cheese", "Bavarian Bergkase",
        "Baylough", "Beaufort", "Beauvoorde", "Beenleigh Blue", "Beer Cheese", "Bel Paese",
        "Bergader", "Bergere Bleue", "Berkswell", "Beyaz Peynir", "Bierkase", "Bishop Kennedy",
        "Blarney", "Bleu d'Auvergne", "Bleu de Gex", "Bleu de Laqueuille",
        "Bleu de Septmoncel", "Bleu Des Causses", "Blue", "Blue Castello", "Blue Rathgore",
        "Blue Vein (Australian)", "Blue Vein Cheeses", "Bocconcini", "Bocconcini (Australian)"
)

/**
 * ViewModel
 * Created by mazaiting on 2018/7/26.
 */
class StudentViewModel : ViewModel() {

  /**
   * 保存数据
   */
  fun save(application: Application) {
    // 获取数据库
    val db = Room.databaseBuilder(application, StudentDatabase::class.java, "PAGING").build()
    // 获取DAO
    val dao = db.studentDao()
    // 创建列表
    val list = mutableListOf<StudentRoom>()
    // 遍历字符串数据，将每一项创建为StudentRoom对象并添加到list列表中
    CHEESE_DATA_STRING.map { list.add(StudentRoom(it)) }
    // 执行插入数据
    Thread(Runnable { dao.insert(list) }).start()
  }
  
  /**
   * 查询所有学生
   */
  fun findAllStudents(application: Application): LiveData<PagedList<StudentRoom>> {
    // 获取数据库
    val db = Room.databaseBuilder(application, StudentDatabase::class.java, "PAGING").build()
    // 获取DAO
    val dao = db.studentDao()
    // 创建list，简化版的
//    val list: LiveData<PagedList<StudentRoom>> =
//            LivePagedListBuilder(dao.findAllStudents(), 15).build()
    // 根据PagedList.Config创建ListData
    val list: LiveData<PagedList<StudentRoom>> =
            LivePagedListBuilder(dao.findAllStudents(),
                    PagedList.Config.Builder()
                            // 设置分页加载数量
                            .setPageSize(PAGE_SIZE)
                            // 配置是否启动PlaceHolders
                            .setEnablePlaceholders(ENABLE_PLACE_HOLDERS)
                            // 初始化加载数量
                            .setInitialLoadSizeHint(PAGE_SIZE)
                            .build()
                    ).build()
    return list
  }
  
  /**
   * 查询本地数据
   */
  fun loadNativeStudent(): LiveData<PagedList<Student>> {
    val dataSourceFactory = object: DataSource.Factory<Int, Student>() {
      override fun create(): DataSource<Int, Student> {
        return StudentDataSource()
      }
    }
//    DataSource.Factory<Int, Student>
    val list: LiveData<PagedList<Student>> =
            LivePagedListBuilder(dataSourceFactory,
                    PagedList.Config.Builder()
                            .setPageSize(PAGE_SIZE)
                            .setEnablePlaceholders(ENABLE_PLACE_HOLDERS)
                            .setInitialLoadSizeHint(PAGE_SIZE)
                            .build()
            ).build()
    return list
  }
  
  companion object {
    // 每一页数据量
    private val PAGE_SIZE = 15
    // 是否开启PlaceHolders
    private val ENABLE_PLACE_HOLDERS = false
  }
}