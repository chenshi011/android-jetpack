package com.mazaiting.jetpack.architecture

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.mazaiting.jetpack.R
import com.mazaiting.jetpack.architecture.databinding.expression.ExpressionActivity
import com.mazaiting.jetpack.architecture.databinding.observable.ObservableActivity
import com.mazaiting.jetpack.architecture.lifecycle.LifeCycleActivity
import com.mazaiting.jetpack.architecture.live.LiveDataActivity
import com.mazaiting.jetpack.architecture.model.ViewModelActivity
import com.mazaiting.jetpack.architecture.navigation.NavigationActivity
import com.mazaiting.jetpack.architecture.paging.PagingActivity
import com.mazaiting.jetpack.architecture.room.RoomActivity
import com.mazaiting.jetpack.architecture.work.WorkManagerActivity
import kotlinx.android.synthetic.main.activity_architecture.*

class ArchitectureActivity : AppCompatActivity() {
  
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_architecture)
    
    // DataBinding
    btn_architecture_expression.setOnClickListener { startActivity(Intent(this, ExpressionActivity::class.java)) }
    // DataBinding
    btn_architecture_observable.setOnClickListener { startActivity(Intent(this, ObservableActivity::class.java)) }
    // Room
    btn_architecture_room.setOnClickListener { startActivity(Intent(this, RoomActivity::class.java)) }
    // ViewModel
    btn_architecture_model.setOnClickListener { startActivity(Intent(this, ViewModelActivity::class.java)) }
    // LiveData
    btn_architecture_live.setOnClickListener { startActivity(Intent(this, LiveDataActivity::class.java)) }
    // WorkManager
    btn_architecture_work.setOnClickListener { startActivity(Intent(this, WorkManagerActivity::class.java)) }
    // Paging
    btn_architecture_paging.setOnClickListener { startActivity(Intent(this, PagingActivity::class.java)) }
    // LifeCycle
    btn_architecture_life_cycle.setOnClickListener { startActivity(Intent(this, LifeCycleActivity::class.java)) }
    // Navigation
    btn_architecture_navigation.setOnClickListener { startActivity(Intent(this, NavigationActivity::class.java)) }
  }
}
