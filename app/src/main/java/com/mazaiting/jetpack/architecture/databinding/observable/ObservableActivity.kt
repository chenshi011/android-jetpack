package com.mazaiting.jetpack.architecture.databinding.observable

import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.mazaiting.jetpack.BR
import com.mazaiting.jetpack.R
import com.mazaiting.jetpack.architecture.databinding.bean.BaseObservableUser
import com.mazaiting.jetpack.databinding.ActivityObservableBinding

class ObservableActivity : AppCompatActivity() {
//  private val user = ObservableUser(ObservableField("ma"), ObservableField("zaiting"), ObservableInt(10))
  private val user = BaseObservableUser("ma", "zaiting", 10)
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    val binding = DataBindingUtil.setContentView<ActivityObservableBinding>(this, R.layout.activity_observable)
    binding.user = user
  }
  
  /**
   * 修改FirstName
   */
  fun onChangeFirstName(view: View) {
    user.firstName = "aaa"
    // 更新数据
    user.notifyPropertyChanged(BR.firstName)
  }
}
