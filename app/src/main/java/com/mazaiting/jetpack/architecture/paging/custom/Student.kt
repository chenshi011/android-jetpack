package com.mazaiting.jetpack.architecture.paging.custom

/**
 * 数据实体类
 * Created by mazaiting on 2018/7/26.
 */
data class Student (
     val id: Int,
     val name: String
)