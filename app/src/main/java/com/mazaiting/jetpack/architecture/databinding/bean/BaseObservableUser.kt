package com.mazaiting.jetpack.architecture.databinding.bean

import android.databinding.BaseObservable
import android.databinding.Bindable

/**
 * Created by mazaiting on 2018/7/24.
 */
data class BaseObservableUser(
        @Bindable
        var firstName: String,
        val lastName: String,
        val age: Int
) : BaseObservable()