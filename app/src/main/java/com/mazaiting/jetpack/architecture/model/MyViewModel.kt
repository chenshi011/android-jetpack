package com.mazaiting.jetpack.architecture.model

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel

/**
 * 自定义ViewModel
 * Created by mazaiting on 2018/7/25.
 */
class MyViewModel : ViewModel() {
  var users: MutableLiveData<List<User>>? = null
    get() {
      if (null == field) {
        users = MutableLiveData()
        loadUsers()
      }
      return field
    }
  
  /**
   * 加载数据
   */
  private fun loadUsers() {
    val userList = arrayListOf<User>()
    (0..10).map { userList.add(User("first$it", "last$it")) }
    users?.value = userList
  }
}