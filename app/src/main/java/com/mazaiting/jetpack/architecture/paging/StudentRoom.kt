package com.mazaiting.jetpack.architecture.paging

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Room 实体
 * Created by mazaiting on 2018/7/25.
 */
@Entity(tableName = "student")
data class StudentRoom(
        var name: String
) {
  @PrimaryKey(autoGenerate = true)
  var id: Int = 0
}