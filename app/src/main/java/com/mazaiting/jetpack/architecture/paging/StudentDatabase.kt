package com.mazaiting.jetpack.architecture.paging

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

/**
 * 数据库
 * Created by mazaiting on 2018/7/26.
 */
@Database(entities = arrayOf(StudentRoom::class), version = 1)
abstract class StudentDatabase : RoomDatabase() {
  // 获取DAO
  abstract fun studentDao() : StudentDao
}