package com.mazaiting.jetpack.architecture.lifecycle.life

import android.arch.lifecycle.LifecycleOwner
import android.util.Log

/**
 * Created by mazaiting on 2018/7/27.
 */
class SpecialPresenter : IPresenter {
  companion object {
    private val TAG = SpecialPresenter::class.java.simpleName
  }
  override fun onCreate(owner: LifecycleOwner) {
    Log.e(TAG, "onCreate")
  }
  
  override fun onDestroy(owner: LifecycleOwner) {
    Log.e(TAG, "onDestroy")
  }
  
  override fun onLifecycleChanged(owner: LifecycleOwner) {
    Log.e(TAG, "onLifecycleChanged")
  }
  
}