package com.mazaiting.jetpack.architecture.navigation

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.mazaiting.jetpack.R

class NavigationActivity : AppCompatActivity() {
  
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_navigation)
    /**
     * 如果布局文件的fragment中没有这两行，则需要设置如下内容
     * app:defaultNavHost="true"
     * app:navGraph="@navigation/nav_graph"
     */
//    val finalHost = NavHostFragment.create(R.navigation.nav_graph)
//    supportFragmentManager.beginTransaction()
//            .replace(R.id.nav_host_fragment, finalHost)
//            .setPrimaryNavigationFragment(finalHost)
//            .commit()
  }
  
  override fun onNavigateUp(): Boolean = findNavController(this, R.id.nav_host_fragment).navigateUp()
}
