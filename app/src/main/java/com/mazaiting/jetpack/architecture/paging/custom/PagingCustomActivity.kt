package com.mazaiting.jetpack.architecture.paging.custom

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.mazaiting.jetpack.R
import com.mazaiting.jetpack.architecture.paging.StudentViewModel
import kotlinx.android.synthetic.main.activity_paging_room.*

class PagingCustomActivity : AppCompatActivity() {
  /**
   * 懒加载ViewModel
   */
  private val viewModel by lazy {
    ViewModelProviders.of(this).get(StudentViewModel::class.java)
  }
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_paging_custom)
    // 创建适配器
    val adapter = StudentAdapter()
    // 设置布局管理者
    rv_show.layoutManager = LinearLayoutManager(this)
    // 设置适配器
    rv_show.adapter = adapter
    // 数据改变通知RecyclerView更新
    viewModel.loadNativeStudent().observe(this, Observer { adapter.submitList(it) })
  }
}
