package com.mazaiting.jetpack.architecture.room

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

/**
 * 数据库类
 * Created by mazaiting on 2018/7/25.
 */
@Database(entities = arrayOf(User::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
  // 获取DAO
  abstract fun userDao(): UserDao
}