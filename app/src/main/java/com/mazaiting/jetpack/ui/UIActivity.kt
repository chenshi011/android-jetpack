package com.mazaiting.jetpack.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.mazaiting.jetpack.R

class UIActivity : AppCompatActivity() {
  
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_ui)
  }
}
